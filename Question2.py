class Node:
  def __init__(self,data):
    self.data = data
    self.next = None

class Linkedlist:
  def __init__(self):
    self.head = None 
  
  def insert_at_begining(self,data):
    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
  
  def insert_at_end(self,data):
    new_node = Node(data)
    temp = self.head 
    while temp.next:
      temp = temp.next  
    temp.next = new_node
  
  def insert_at_position(self,pos,data):
    new_node = Node(data)
    temp = self.head 
    for i in range(pos-1):
      temp = temp.next 
    new_node.data = data 
    new_node.next = temp.next
    temp.next = new_node
  
  def delete_at_begining(self):
    temp = self.head 
    self.head = temp.next 
    temp.next = None
  
  def delete_at_end(self):
    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None
  
  def delete_at_position(self,pos):
    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next
  
  def display(self):
    if self.head is None:
      print("Empty LinkedList")
    else:
      temp = self.head 
      new = ""
      while(temp):
        new+=temp.data
        temp=temp.next 
      return new

def is_balanced(expr):
	stack = []

	# Traverse the Expression
	for char in expr:
		if char in ["(", "{", "["]:
			stack.append(char)
		else:

			if not stack:
				return False
			current_char = stack.pop()
			if current_char == '(':
				if char != ")":
					return False
			if current_char == '{':
				if char != "}":
					return False
			if current_char == '[':
				if char != "]":
					return False
	if stack:
		return False
	return True

l = Linkedlist()
n = Node('(')
l.head = n
n1 = Node('{')
n.next = n1 
n2 = Node('}')
n1.next = n2 
n3 = Node(')')
n2.next = n3 
l.insert_at_begining('[')
l.insert_at_end(']')
l.delete_at_end()
x = l.display()

expr = x
print(expr)

#call the function
if is_balanced(expr):
	print("Balanced")
else:
	print("Not Balanced")
